"use strict";
// 1. Чтоб огласить переменную в JS нужно использовать ключевое слово let или const, так же есть устаревшее var (уже наверное не используется).
// 2. Функция prompt выводит модальное окно с текстом, и полем для ввода текста с кнопками "ОК" и "Отмена". Пользователь вводит текст в окно ввода и нажимает "ОК", который присваивается
// переменной(если мы ее обьявили), если нажимает "Отмена" то приcваивается значение null, если ничего не введет в поле, то вернет пустую строку ("").
// В свою очередь функция confirm выводит модальное окно с текстом вопроса и тоже двумя кнопками "ОК" и "Отмена", если пользователь отвечает на вопрос "ОК" то результат true, если
// "Отмена" то false (если вкратце то, функция confirm ставит перед пользователем выбор между двумя кнопками, а prompt просит ввести текст и возвращает его, если нажимает кнопу "ОК"). 
// 3. Неявное преобразование типов, это то преобразование, что произошло как последствие каких-то действий, когда в выражениях используют значения различных типов. (10/'5', +[])

const name = 'Alexander';
const admin = name; 
// let admin = name;
console.log (admin);

let days = 8;
const daysInSeconds = 60 * 60 * 24 * days; 
console.log (daysInSeconds);

let userName = prompt('Как Вас зовут?', 'Мой господин');
if (userName === 'Мой господин') {
    alert('Привет, босс!')
} else if (userName === null) {
    alert('Привет, неведомый гость')
} else {
    alert(`Привет, ${userName}`)
}
console.log(userName);